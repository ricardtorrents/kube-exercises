# Ejercicio 1. [Ingress Controller / Secrets] 
## Crea los siguientes objetos de forma declarativa con las siguientes especificaciones:
• Imagen: nginx
• Version: 1.19.4
• 3 replicas
• Label: app: nginx-server
• Exponer el puerto 80 de los pods
• Limits:
CPU: 20 milicores Memoria: 128Mi
• Requests:
CPU: 20 milicores Memoria: 128Mi

* A continuación, tras haber expuesto el servicio en el puerto 80, se deberá acceder a la página principal de Nginx a través de la siguiente URL:
    Primero se ha creado servicio i el deployment que se pide, se puede ver la declaración de estos en los ficheros nginx-deployment.yaml y nginx-server-service.yaml+
    Se han arrancado con el comando:
    ```console
        ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (master)> kubectl apply -f nginx-deployment.yaml 
        ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (master)> kubectl apply -f nginx-server-service.yaml 
    ```

    Seguidamente se ha creado un Ingress controller para poder acceder al servicio sin tener que especificar el puerto:
    Se puede ver la definición en el nginx-server-ingress-v1.yaml se ha definido de nombre del host ricard.student.lasalle.com
   Se ha ejecutado con:
    ```console
        ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (master)> kubectl apply -f nginx-server-ingress-v1.yaml 
    ```

    Para poder probar esto se ha tenido que especificar en nuestro fichero hosts que el nombre ricard.student.lasalle.com se assigne a la ip del minikube
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~> cd /etc/
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard /etc> sudo nano hosts

    192.168.64.2 ricard.student.lasalle.com
    ```
    Con esto ya se puede acceder por el navegador a nuestro servicio.
    [screen-shot-1.png]

* Una vez realizadas las pruebas con el protocolo HTTP, se pide acceder al servicio mediante la utilización del protocolo HTTPS, para ello:
    • Crear un certificado mediante la herramienta OpenSSL u otra similar
    • Crear un secret que contenga el certificado

    Para esto el primer paso ha estado crear los certificados con el comando openssl
    ```console
        MacBook-MacBook-Pro-de-Ricard:kube-exercises ricardtorrents$ openssl req -newkey rsa:2048 -nodes -keyout ricardstudentslasalle.key -x509 -days 365 -out ricardstudentslasalle.cr
    ```

    Seguidamente se ha tenido que transformar cada uno a base64
    ```console
    MacBook-MacBook-Pro-de-Ricard:kube-exercises ricardtorrents$ cat ricardstudentslasalle.key | base64
    MacBook-MacBook-Pro-de-Ricard:kube-exercises ricardtorrents$ cat ricardstudentslasalle.crt | base64
    ```

    Después se ha creado un Secret, como se puede ver en el archivo nginx-secret.yaml y se ha arrancado
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (master)> kubectl apply -f nginx-secret.yaml 
    ```
    Después se ha modificado el Ingress Controller para que use el secret como se puede ver en el archivo nginx-server-ingress-v2.yaml 
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (master)> kubectl apply -f nginx-server-ingress-v2.yaml 
    ```

    En este momento ya se puede acceder a nuestro servicio por https

    [screen-shot-2.png]

