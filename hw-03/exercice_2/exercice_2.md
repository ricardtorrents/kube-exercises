# Ejercicio 2. 
## [StatefulSet] Crear un StatefulSet con 3 instancias de MongoDB (ejemplo visto en clase)
Se pide:
• Habilitar el clúster de MongoDB
• Realizar una operación en una de las instancias a nivel de configuración y
verificar que el cambio se ha aplicado al resto de instancias
• Diferencias que existiría si el montaje se hubiera realizado con el objeto de
ReplicaSet

* Primero se ha definido el servicio como se puede ver en el archivo mongodb-service.yaml, seguidamente se ha definido el objeto Stateful como se puede ver en el archivo mongodb-stateful.yaml
En este objeto se ha definido de tipo stateful que cree 3 replicas i con contenedores mongodb que se configuraran en modo replicaset para trabajar juntos. Para esta prueba no se ha definido ningún volumen de persistencia concreto.
 
* El siguiente paso a sido arrancar los dos objetos:
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_2 (master)> kubectl apply -f mongodb-service.yaml 
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_2 (master)> kubectl apply -f mongodb-stateful.yaml 
    ```
* Una ves arrancados se ha podido ver como se ha creado el objeto statefulSet:
 ```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_2 (master)> kubectl get statefulset
NAME               READY   AGE
mongodb-stateful   3/3     40m
```
Para configurar se ha tenido que iniciar en cada uno de los pods las otras instancias del cluster entrando dentro de cada una:
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_2 (master)> kubectl exec -ti mongodb-stateful-1 -- mongo
```
i ejecutar el comando rs.initate:

````console
rs.initiate({ _id:"mongoReplica",version:1,members:[{_id:0, host:"mongodb-stateful-0.mongodb-service.default.svc.cluster.local:27017"},{_id:1, host:"mongodb-stateful-1.mongodb-service.default.svc.cluster.local:27017"},{_id:2, host:"mongodb-stateful-2.mongodb-service.default.svc.cluster.local:27017"}]})
```   

* Una vez realizado este comando en las tres instancias se ha definido un pod como primary(master ) y los otros dos como secondarys (slaves), realizando una modificación en el pod primary podemos ver la en los otros dos pods.

[screenshot-1-insertOnPrimary.png, screenshot-2-view-insecondary.png]

