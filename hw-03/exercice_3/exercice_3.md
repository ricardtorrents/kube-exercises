# Ejercicio 3. 
## [Horizontal Pod Autoscaler] Crea un objeto de kubernetes HPA, que escale a partir de las métricas CPU o memoria (a vuestra elección). Establece el umbral al 50% de CPU/memoria utilizada, cuando pase el umbral, automáticamente se deberá escalar al doble de replicas.
* Podéis realizar una prueba de estrés realizando un número de peticiones masivas mediante la siguiente instrucción:
    ```console
    kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://<svc_name>; done"
    ````

* Para realizar este ejercicio se ha usado un deployment de nginx que esta definido en el nginx-deployment.yaml, que generaba un replicaset de 1 sola replica.
También se ha expuesto el servicio al exterior con un servicio definido en el nginx-hpa-service.yaml para poder realizar las pruebas de carga.

* Seguidamente se ha definido el objeto HorizontalPodAutoscaler en el fichero nginx-server-hpa.yaml, se le ha especificado que usara el deployment nginx-server-hpa-deployment y que pusiera un umbral de memoria del 50% y un umbral de cpu del 50%, se ha definido un maximo de 5 replicas.

* Se arrancan los objetos:
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl apply -f nginx-deployment.yaml 
    deployment.apps/nginx-server-hpa-deployment created
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl apply -f nginx-hpa-service.yaml 
    service/service-nginx-hpa created
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl apply -f nginx-server-hpa.yaml 
    horizontalpodautoscaler.autoscaling/nginx-server-hpa created

    ```

* Si se observa se va a ver que solo hay un pod del deployment tal i como se ha definido:

    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl get deployments
    NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
    nginx-server-hpa-deployment   1/1     1            1           4m31s
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl get pods
    NAME                                           READY   STATUS    RESTARTS   AGE
    nginx-server-hpa-deployment-6fc9569c4d-mlqw6   1/1     Running   0          4m33s
    ```

* Si se observa el  HorizontalPodAutoscaler: 
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl get HorizontalPodAutoscaler
    NAME               REFERENCE                                TARGETS          MINPODS   MAXPODS   REPLICAS   AGE
    nginx-server-hpa   Deployment/nginx-server-hpa-deployment   2%/50%, 0%/50%   1         5         1          5m3s
    ```
* Se puede observar que solo hay una replica de un maximo de 5 i el estado de los umbrales definidos en Targets.

Si arrancamos el pod load-generator del enunciado, se va a ver como sube la memoria i acaba auto escalando con mas replicas.

    ```console
    MacBook-MacBook-Pro-de-Ricard:exercice_2 ricardtorrents$ kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://192.168.64.2:30500/; done"
    ```
* Si observamos los diferentes objetos va cambiando.
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl get HorizontalPodAutoscaler
    NAME               REFERENCE                                TARGETS          MINPODS   MAXPODS   REPLICAS   AGE
    nginx-server-hpa   Deployment/nginx-server-hpa-deployment   2%/50%, 0%/50%   1         5         1          8m33s
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl get HorizontalPodAutoscaler
    NAME               REFERENCE                                TARGETS           MINPODS   MAXPODS   REPLICAS   AGE
    nginx-server-hpa   Deployment/nginx-server-hpa-deployment   2%/50%, 90%/50%   1         5         1          8m47s
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl get HorizontalPodAutoscaler
    NAME               REFERENCE                                TARGETS           MINPODS   MAXPODS   REPLICAS   AGE
    nginx-server-hpa   Deployment/nginx-server-hpa-deployment   2%/50%, 90%/50%   1         5         2          8m53s

    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl get deployments
    NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
    nginx-server-hpa-deployment   2/2     2            2           10m
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> kubectl get pods
    NAME                                           READY   STATUS    RESTARTS   AGE
    load-generator                                 1/1     Running   0          3m
    nginx-server-hpa-deployment-6fc9569c4d-7pq7l   1/1     Running   0          107s
    nginx-server-hpa-deployment-6fc9569c4d-mlqw6   1/1     Running   0          10m
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_3 (master)> 

    ```
[screenshot-1.png, screenshot-2.png]

