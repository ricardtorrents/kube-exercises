# Ejercicio2

## Crear un objeto de tipo replicaSet a partir del objeto anterior con las siguientes especificaciones:
* Debe tener 3 replicas
 En el fichero nginx-server-replicaset.yaml se ha declarado el objeto replicaset.
 Con el comando create lo hemos arrancado
 ```console
 ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_2 (development)> 
kubectl create -f nginx-server-replicaset.yaml

 ``` 
* ¿Cual sería el comando que utilizarías para escalar el número de replicas a
10?
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_2 (development)> 
kubectl scale replicaset nginx-server-replicaset --replicas=10
```
* Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? (No es necesario adjuntar el objeto)
 El objeto adecuado seria un DaemonSet, que asegura una replica en cada uno de los nodos.

