# Ejercicio 4
## Crear un objeto de tipo deployment con las especificaciones del ejercicio 1.
    Para este ejercicio usamos la imagen de nginx nginx:1.19.4-alpine y luego tenemos una imagen propia que llamada custom-nginx-version:1.0 que usa la misma imagen pero tiene alguna diferencia para hacer una versión diferente.
* Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate”
    Para esto hemos configurado dos ficheros .yaml dentro del directorio Recreate nginx-server-deployment-recreate-v1.yml y el nginx-server-deployment-recreate-v2.yml. En estos ficheros se especifica un deployment con los objetos de los ejercicios anteriores. En la versión dos simplemente s e modifica la imagen.
    El tipo de estrategia definida para el despliegue es "Recreate".
    Ejecutamos el comando:
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/e/Recreate (development)> 
    kubectl apply -f nginx-server-deployment-recreate-v1.yaml
    ```
    En otro terminal ejecutamos
    ```console
    (base) MacBook-MacBook-Pro-de-Ricard:entregable_docker ricardtorrents$ while :; do clear; kubectl get pods;sleep 2; done
    ```
    Para poder ver como se efectúa el despliegue en tiempo real.

    Para efectuar el despliegue de la segunda versión:
     ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/e/Recreate (development)> 
    kubectl apply -f nginx-server-deployment-recreate-v2.yaml
    ```

    En el otro terminal donde tenemos corriendo el bucle con el get pods, veremos que los pods arrancados se apagan y seguidamente se levantan los nuevos pods con la versión anterior.

* Despliega una nueva versión haciendo “rollout deployment”
    Como en el apartado anterior tenemos en la carpeta RollingUpdate dos archivos yaml como los del ejercicio anterior, pero esta vez la estrategia es RollingUpdate.
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/e/RollingUpdate (development)> 
    kubectl apply -f nginx-server-deployment-rollingUpdate-v1.yaml
    ``` 
    En otro terminal ejecutamos
    ```console
    (base) MacBook-MacBook-Pro-de-Ricard:entregable_docker ricardtorrents$ while :; do clear; kubectl get pods;sleep 2; done
    ```
    Para poder ver como se efectúa el despliegue en tiempo real.

    Para efectuar el despliegue de la segunda versión:
      ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/e/RollingUpdate (development)> 
    kubectl apply -f nginx-server-deployment-rollingUpdate-v2.yaml
    ```
    Esta vez vemos como se levantan otros 3 pods ( teníamos definidos 3 replicas) y no se apagan los 3 primeros hasta que se han levantado y están funcionando los 3 nuevos pods con la nueva versión. Una vez funcionando se empiezan a apagar los antiguos.  

* Realiza un rollback a la versión generada previamente
    Para realizar un rollback i volver a la versión anterior del deployment con RollingUpdate vamos a ejecutar el siguiente comando
    ```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/e/RollingUpdate (development)> 
    kubectl rollout history deployment nginx-server-deployment
    ```

**En los archvios Recreate.mov y RollingUpdate_with_rollback.mov dentro del archivo video.zip subido al campus se puede ver el todo el proceso.**
