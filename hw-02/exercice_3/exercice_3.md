# Ejercicio 3
## Crea un objeto de tipo service para exponer la aplicación del ejercicio anterior de las siguientes formas:
* Exponiendo el servicio hacia el exterior (crea service1.yaml)
    Para este modo hemos creado el servicio del fichero service_1.yaml donde hemos especificado que seria de tipo NodePort, sin especificar ningún puerto, por lo tanto se exponía un puerto aleatorio dentro del rango (30000-32767) i se podía acceder con la ip de minikube.
* De forma interna, sin acceso desde el exterior (crea service2.yaml)
    Para este modo hemos creado el servicio del fichero service_2.yaml donde no le hemos especificado de que tipo era, por lo tanto era de tipo ClusterIP y no se podía acceder des de el exterior
* Abriendo un puerto especifico de la VM (crea service3.yaml)
    Para este hemos usado el tipo de servicio NodePort pero esta vez hemos especificado con la opción nodePort un puerto dentro del rango 30000-32767

Hemos arrancado cada uno con el comando create y si ejecutamos el comando get sercices de kubernetes podemos ver los servicios que hemos levantado:
```console
ricardtorrents@MacBookdeRicard ~/D/M/P/k/k/hw-02 (development)> kubectl get service
NAME              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
kubernetes        ClusterIP   10.96.0.1        <none>        443/TCP        20m
service-nginx-1   NodePort    10.98.46.56      <none>        80:30182/TCP   13m
service-nginx-2   ClusterIP   10.109.152.172   <none>        80/TCP         12m
service-nginx-3   NodePort    10.109.204.135   <none>        80:30500/TCP   6m43s

```
Si usamos el comando get endpoints podemos ver como estos servicios están mapeados con las replicas del replicaset
```console
ricardtorrents@MacBookdeRicard ~/D/M/P/k/k/hw-02 (development)> kubectl get endpoints
NAME              ENDPOINTS                                    AGE
kubernetes        192.168.64.2:8443                            21m
service-nginx-1   172.17.0.10:80,172.17.0.8:80,172.17.0.9:80   14m
service-nginx-2   172.17.0.10:80,172.17.0.8:80,172.17.0.9:80   12m
service-nginx-3   172.17.0.10:80,172.17.0.8:80,172.17.0.9:80   7m23s
```

