# Ejercicio 5
## Diseña una estrategia de despliegue que se base en ”Blue Green”. Podéis utilizar la imagen del ejercicio 1.
Para este ejercicio hemos implementado dos deployments como en el ejercicio anterior, el deployment definido en el yaml "nginx-server-deployment-blue.yaml" se va a llamar nginx-server-deployment-blue-green-v1 y el definido en el yaml "nginx-server-deployment-green.yaml" se va a llamar nginx-server-deployment-blue-green-v2.

Los dos tienen las mismas configuraciones, no se le ha definido ninguna estrategia de despliegue ya que implementaremos nosotros le blue-green.

El replicaset que se configura en el nginx-server-deployment-blue-green-v1 se le especifica que corresponde a la versión 1.0.0, el repolicaset que se configura en el nginx-server-deployment-blue-green-v2 se le especifica que es la versión 2.0.0.
Se especifican las versiones del replicaset por que vamos a definir un servicio que apunte al replicaset de versión 1.0.0 que definiremos en el archivo "service-blue.yaml" y se llamará service-blue-green y una actualización de este en el archivo "service-green.yaml que apuntará al replicaset con versión 2.0.0, esto nos permitirá redirigir el trafico en el momento adecuado.

Una vez están todos los archivos se levanta el primer deployment .
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_5 (development)> kubectl apply -f nginx-server-deployment-blue.yaml 

```
Seguidamente levantamos el servicio servicio para exponer la primera versión

```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_5 (development)> kubectl apply -f service-blue.yaml 
```

En este momento podemos ver el contenido de la versión 1 si nos dirigimos al navegador y vamos a localhost:30500

Levantamos el segundo deployment:
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_5 (development)> kubectl apply -f nginx-server-deployment-green.yaml 
```
 Si realizamos el comando kubectl get pods vemos que tenemos los pods de los dos deployments funcionando.

```console
    ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_5 (development)> kubectl get pods 
    NAME                                                     READY   STATUS    RESTARTS   AGE
    nginx-server-deployment-blue-green-v1-d86fd84b-7wdjv     1/1     Running   0          15s
    nginx-server-deployment-blue-green-v1-d86fd84b-clhjm     1/1     Running   0          15s
    nginx-server-deployment-blue-green-v1-d86fd84b-d5s86     1/1     Running   0          15s
    nginx-server-deployment-blue-green-v2-79656d8fd9-6dk6r   1/1     Running   0          8s
    nginx-server-deployment-blue-green-v2-79656d8fd9-f4q9m   1/1     Running   0          8s
    nginx-server-deployment-blue-green-v2-79656d8fd9-x24vq   1/1     Running   0          8s
```
También podemos ejecutar un comando para ver si el segundo servicio se ha desplegado con éxito :
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_5 (development) [1]> kubectl rollout status deploy nginx-server-deployment-blue-green-v2 -w
deployment "nginx-server-deployment-blue-green-v2" successfully rolled out
```
    
Llegados a este punto es el momento de que nuestros desarrolladores/testers prueben la aplicación con la versión 2 que solo se puede acceder internamente. 
 Una vez funciona lanzamos la actualización del servicio para que apunte en lugar de la versión 1 a la versión dos 

   
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_5 (development)> kubectl apply -f service-green.yaml 
```
    
En estos momentos se puede acceder des de fuera a la versión 2 y el ultimo paso es apagar la versión 1

```console
 ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_5 (development)> kubectl delete deployment nginx-server-deployment-blue-green-v1
    deployment.apps "nginx-server-deployment-blue-green-v1" deleted
```

**En el archivo Blue-green.mov  dentro del archivo video.zip subido al campus se puede ver el todo el proceso.**
