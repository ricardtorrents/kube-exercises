# Ejercicio 1
## Crea un pod de forma declarativa con las siguientes especificaciones:
• Imagen: nginx
• Version: 1.19.4
• Label: app: nginx-server
• Limits
CPU: 100 milicores
Memoria: 256Mi
• Requests
CPU: 100 milicores Memoria: 256Mi

Hemos creado el archivo nginx-server-pod.yaml y hemos ejecutado el comando create para arrancar-lo
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)> kubectl create -f  nginx-server-pod.yaml
```
* ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)?

```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)>  kubectl logs --tail=10 nginx-server
```
• ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías.
    Una opción es usar el comando get pods con la opción -wide 
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)> kubectl get pods -o wide
NAME           READY   STATUS    RESTARTS   AGE   IP           NODE   NOMINATED NODE   READINESS GATES
nginx-server   1/1     Running   0          49m   172.17.0.7   m01    <none>           <none>
```
    Otra opción es usar el comando describe para que te describa el pod 
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)> 
kubectl describe pod nginx-server
Name:         nginx-server
Namespace:    default
Priority:     0
Node:         m01/192.168.64.2
Start Time:   Sun, 15 Nov 2020 18:12:29 +0100
Labels:       name=nginx-server
Annotations:  <none>
Status:       Running
IP:           172.17.0.7
IPs:
  IP:  172.17.0.7
Containers:
  nginx-server:
    Container ID:   docker://cd55714f5fc72b49bf19ff4d7a0466e70b0aaab93f1aa6e1da32156188f139a5
    Image:          nginx:1.19.4-alpine
    Image ID:       docker-pullable://nginx@sha256:9b22bb6d703d52b079ae4262081f3b850009e80cd2fc53cdcb8795f3a7b452ee
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Sun, 15 Nov 2020 18:12:30 +0100
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-wq58l (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-wq58l:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-wq58l
    Optional:    false
QoS Class:       Guaranteed
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason     Age        From               Message
  ----    ------     ----       ----               -------
  Normal  Scheduled  <unknown>  default-scheduler  Successfully assigned default/nginx-server to m01
  Normal  Pulled     51m        kubelet, m01       Container image "nginx:1.19.4-alpine" already present on machine
  Normal  Created    51m        kubelet, m01       Created container nginx-server
  Normal  Started    51m        kubelet, m01       Started container nginx-server
```
Screenshots:
- screenShoot_1
- screenShoot_2
* ¿Qué comando utilizarías para entrar dentro del pod?
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)> 
kubectl exec -ti nginx-server sh
```
* Necesitas visualizar el contenido que expone NGINX, ¿qué acciones
debes llevar a cabo?
    Es necesario exponer el pod, para esto se usará el comando expose de kubernetes, que va a crear un servicio para exponer el pod, le especificaremos que cree un servicio de tipo NodePort.
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)> 
    kubectl expose pod nginx-server --name nginx--server-service \
    --type NodePort
```
Con el comando get services podremos ver que servicios tenemos y que puertos exponen.
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)> kubectl get services
NAME                    TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
kubernetes              ClusterIP   10.96.0.1     <none>        443/TCP        234d
nginx--server-service   NodePort    10.96.53.15   <none>        80:32578/TCP   36m
```
Seguidamente podemos usar el comando minikube ip para ver la ip de minikube
```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)> minikube ip
192.168.64.2
```
Con la ip i el puerto podemos abrir el navegador i ver acceder al contenido de la aplicación
Screenshots:
- screenShoot_3

• Indica la calidad de servicio (QoS) establecida en el pod que acabas de
crear. ¿Qué lo has mirado?

    QoS Class:       Guaranteed
    He usado el comando describe para describir el pod i ver que QoS marcaba.

```console
ricardtorrents@MacBook-MacBook-Pro-de-Ricard ~/D/M/P/k/k/h/exercice_1 (development)> 
kubectl describe pod nginx-server
Name:         nginx-server
Namespace:    default
Priority:     0
Node:         m01/192.168.64.2
Start Time:   Sun, 15 Nov 2020 18:12:29 +0100
Labels:       name=nginx-server
Annotations:  <none>
Status:       Running
IP:           172.17.0.7
IPs:
  IP:  172.17.0.7
Containers:
  nginx-server:
    Container ID:   docker://cd55714f5fc72b49bf19ff4d7a0466e70b0aaab93f1aa6e1da32156188f139a5
    Image:          nginx:1.19.4-alpine
    Image ID:       docker-pullable://nginx@sha256:9b22bb6d703d52b079ae4262081f3b850009e80cd2fc53cdcb8795f3a7b452ee
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Sun, 15 Nov 2020 18:12:30 +0100
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-wq58l (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-wq58l:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-wq58l
    Optional:    false
QoS Class:       Guaranteed
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason     Age        From               Message
  ----    ------     ----       ----               -------
  Normal  Scheduled  <unknown>  default-scheduler  Successfully assigned default/nginx-server to m01
  Normal  Pulled     51m        kubelet, m01       Container image "nginx:1.19.4-alpine" already present on machine
  Normal  Created    51m        kubelet, m01       Created container nginx-server
  Normal  Started    51m        kubelet, m01       Started container nginx-server
```

